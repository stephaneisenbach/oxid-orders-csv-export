<?php
/**
 * Copyright (c) 2020 Stephan Eisenbach <info@stephan-eisenbach.de>
 */

namespace StephanEisenbach\OrdersCsvExport\Controller\Admin;

use OxidEsales\Eshop\Application\Controller\Admin\AdminDetailsController;
use OxidEsales\Eshop\Core\DatabaseProvider;
use OxidEsales\Eshop\Core\Registry;

class OrdersExportController extends AdminDetailsController
{
    public function render()
    {
        parent::render();

        $oDb = DatabaseProvider::getDb();

        $sShopId = Registry::getConfig()->getShopId();
        $sLastOrderNr = $oDb->getOne("SELECT OXORDERNR FROM oxorder WHERE OXSHOPID=? ORDER BY OXORDERNR DESC LIMIT 1", [$sShopId]);
        $sLastOrderDate = $oDb->getOne("SELECT OXORDERDATE FROM oxorder WHERE OXSHOPID=? ORDER BY OXORDERNR DESC LIMIT 1", [$sShopId]);
        $sDateString = substr($sLastOrderDate, 0, 10);
        $sFirstOrderNrSameDay = $oDb->getOne("SELECT OXORDERNR FROM oxorder WHERE OXORDERDATE>=? && OXSHOPID=? ORDER BY OXORDERNR ASC LIMIT 1", [$sDateString, $sShopId]);
        $this->_aViewData['sLastOrderNr'] = $sLastOrderNr;
        $this->_aViewData['sFirstOrderNrSameDay'] = $sFirstOrderNrSameDay;

        return "seorderscsvexport_orders_export.tpl";
    }

    public function export()
    {

        $oDb = DatabaseProvider::getDb(DatabaseProvider::FETCH_MODE_ASSOC);

        $from = (int) Registry::getRequest()->getRequestParameter('from');
        $to = (int) Registry::getRequest()->getRequestParameter('to');
        $sShopId = (int) Registry::getConfig()->getShopId();

        $aExport = $oDb->getAll("SELECT 
        oxorder.OXORDERDATE as Datum, 
        oxorder.OXORDERNR as Bestellnummer,
       
        oxorder.OXBILLCOMPANY as Rechnungsadresse_Firma,
        oxorder.OXBILLEMAIL as E_Mail,
        oxorder.OXBILLFNAME as Rechnungsadresse_Vorname,
        oxorder.OXBILLLNAME as Rechnungsadresse_Nachname,
        oxorder.OXBILLSTREET as Rechnungsadresse_Straße,
        oxorder.OXBILLSTREETNR as Rechnungsadresse_Hausnummer,
        oxorder.OXBILLADDINFO as Rechnungsadresse_Zusatz,
        oxorder.OXBILLCITY as Rechnungsadresse_Ort,
        billoxcountry.OXTITLE as Rechnungsadresse_Land,
        oxorder.OXBILLZIP as Rechnungsadresse_PLZ,
        oxorder.OXBILLFON as Rechnungsadresse_Telefon,
        oxorder.OXBILLSAL as Rechnungsadresse_Anrede,
        
        oxorder.OXDELCOMPANY as Lieferadresse_Firma,
        oxorder.OXDELFNAME as Lieferadresse_Vorname,
        oxorder.OXDELLNAME as Lieferadresse_Nachname,
        oxorder.OXDELSTREET as Lieferadresse_Straße,
        oxorder.OXDELSTREETNR as Lieferadresse_Hausnummer,
        oxorder.OXDELADDINFO as Lieferadresse_Zusatz,
        oxorder.OXDELCITY as Lieferadresse_Ort,
        deloxcountry.OXTITLE as Lieferadresse_Land,
        oxorder.OXDELZIP as Lieferadresse_PLZ,
        oxorder.OXDELFON as Lieferadresse_Telefon,
        oxorder.OXDELSAL as Lieferadresse_Anrede
       
        FROM oxorder 
        LEFT JOIN oxcountry as billoxcountry ON billoxcountry.OXID = oxorder.OXBILLCOUNTRYID
        LEFT JOIN oxcountry as deloxcountry ON deloxcountry.OXID = oxorder.OXDELCOUNTRYID
        WHERE OXSHOPID=? && OXORDERNR>=? && OXORDERNR<=?", [$sShopId, $from, $to]);

        header('Content-Type: application/csv');
        header('Content-Disposition: attachment; filename="export.csv";');
        $f = fopen('php://output', 'w');

        $aKeys = array_keys($aExport[0]);
        fputcsv($f, $aKeys, "\t");

        foreach ($aExport as $line) {
            fputcsv($f, $line, "\t");
        }

        rewind($f);
        fpassthru($f);
        exit;
    }
}