[{include file="headitem.tpl" title="GENERAL_ADMIN_TITLE"|oxmultilangassign}]
<h1>CSV Export für Bestellungen</h1>
<form name="myedit" id="myedit" action="[{ $oViewConf->getSelfLink() }]" enctype="multipart/form-data" method="post"
      style="padding: 0px;margin: 0px;height:0px;">
    [{$oViewConf->getHiddenSid()}]
    <input type="hidden" name="cl" value="seorderscsvexport_admin_OrdersExportController">
    <input type="hidden" name="fnc" value="export">
    <h3>Bestellnummer</h3>
    <p>
        <label for="from">von</label>
        <input type="text" name="from" id="from" value="[{$sFirstOrderNrSameDay}]">
        <label for="to">bis</label>
        <input type="text" name="to" id="to" value="[{$sLastOrderNr}]">
    </p>
    <p>
        <input type="submit" value="Export">
    </p>
</form>
[{include file="bottomitem.tpl"}]