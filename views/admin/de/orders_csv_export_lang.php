<?php
/**
 * Copyright (c) 2019 Stephan Bauer Webentwicklung <info@stephanbauer.org>
 */

$sLangName = 'Deutsch';

$aLang = array(
    'charset' => 'UTF-8',
    'mx_seorderscsvexport' => 'CSV Export',
);