<?php
/**
 * Copyright (c) 2020 Stephan Eisenbach <info@stephan-eisenbach.de>
 */

/**
 * Metadata version
 * @see https://docs.oxid-esales.com/developer/en/6.1/modules/skeleton/metadataphp/version21.html Documentation of Metadata version.
 */
$sMetadataVersion = '2.1';

/**
 * Module information
 */
$aModule = [
    'id' => 'seorderscsvexport',
    'title' => 'CSV Export für Bestellungen',
    'description' => array(
        'de' => 'CSV Export für Bestellungen.',
        'en' => 'CSV export for orders',
    ),
    'version' => '1.0.0',
    'author' => 'Stephan Eisenbach',
    'url' => 'https://stephan-eisenbach.de',
    'email' => 'info@stephan-eisenbach.de',
    'controllers' => [
        'seorderscsvexport_admin_OrdersExportController' => \StephanEisenbach\OrdersCsvExport\Controller\Admin\OrdersExportController::class,
    ],
    'templates' => [
        'seorderscsvexport_orders_export.tpl' => 'stephaneisenbach/orders_csv_export/views/admin/tpl/seorderscsvexport_orders_export.tpl',
    ]
];